import 'package:flutter/material.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';
import 'package:waiza/src/views/widgets/button.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 48, bottom: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Sélectionnez un profil",
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(fontWeight: FontWeight.w500, color: Colors.black),
              ),
              SizedBox(
                height: 48,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    profileCard(context, "Client", "images/client.jpg"),
                    SizedBox(
                      width: 16,
                    ),
                    profileCard(context, "Conducteur", "images/driver.jpg"),
                  ],
                ),
              ),
              Spacer(),
              PrimaryButton("quitter"),
            ],
          ),
        ),
      ),
    );
  }
}

Widget profileCard(BuildContext context, String title, String image) {
  return InkWell(
    onTap: () {
      Helpers.goto(context, ClientHomeView(), isReplaced: true);
    },
    child: Container(
      width: Helpers.deviceSize(context).width * .6,
      height: Helpers.deviceSize(context).height * .5,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(.5), BlendMode.overlay),
              fit: BoxFit.cover,
              image: AssetImage(image))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 24),
          ),
          SizedBox(height: 8),
          Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis at in facilisi amet at etiam.",
            style: TextStyle(color: Colors.white),
          )
        ],
      ),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';

class PrimaryButton extends StatelessWidget {
  final String title;
  final Function action;
  PrimaryButton(this.title, {this.action});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 48,
      child: RaisedButton(
        disabledColor: Colors.grey,
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        onPressed: action,
        color: ThemeColor.mainColor,
        child: Text(
          title.toUpperCase(),
          style:
              Theme.of(context).textTheme.button.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}

class SecondaryButton extends StatelessWidget {
  final String title;
  final double width;
  final Function action;
  SecondaryButton(this.title, {this.width = double.infinity, this.action});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: 48,
      child: RaisedButton(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        onPressed: action ?? () {},
        color: ThemeColor.secondaryColor,
        child: Text(
          title.toUpperCase(),
          style:
              Theme.of(context).textTheme.button.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}

class MutedButton extends StatelessWidget {
  final String title;
  final double width;
  final IconData icon;
  final Function action;
  MutedButton(this.title, this.icon,
      {this.width = double.infinity, this.action});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: 48,
      child: RaisedButton(
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: action ?? () {},
          color: Colors.black.withOpacity(.5),
          child: Row(
            children: [
              Icon(
                icon,
                color: Colors.white,
              ),
              SizedBox(
                width: 16,
              ),
              Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: Colors.white),
              ),
            ],
          )),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';

class PolylinePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var markers = <Marker>[
      Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(51.5, -0.09),
        builder: (ctx) => Container(
            child: Icon(
          Icons.location_on,
          size: 44,
          color: Colors.red,
        )),
      ),
    ];

    return ViewModelBuilder.reactive(
        builder: (context, model, child) {
          return Scaffold(
            body: Stack(
              children: [
                FlutterMap(
                  options: MapOptions(
                      center: LatLng(51.5, -0.09),
                      zoom: 5.0,
                      onTap: (position) {
                        print(position);
                      }),
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                      subdomains: ['a', 'b', 'c'],
                      tileProvider: CachedNetworkTileProvider(),
                    ),
                    MarkerLayerOptions(markers: markers)
                  ],
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      color: Colors.white.withOpacity(0.8),
                    ),
                    width: Helpers.deviceSize(context).width,
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        input("Ma position actuelle",
                            isEnabled: false, prefixIcon: Icons.my_location),
                        SizedBox(
                          height: 8,
                        ),
                        input("Ma destination", prefixIcon: Icons.navigation),
                        SizedBox(
                          height: 8,
                        ),
                        PrimaryButton("Prendre un taxi")
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
        viewModelBuilder: null);
  }
}

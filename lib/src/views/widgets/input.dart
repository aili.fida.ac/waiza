import 'package:flutter/material.dart';

TextField input(String placeholder,
    {bool isPhone = false,
    bool isObscure = false,
    Function onChange,
    isEnabled = true,
    Function onTap,
    TextCapitalization capitalization = TextCapitalization.none,
    IconData prefixIcon,
    IconButton suffixIcon}) {
  return TextField(
    keyboardType: isPhone ? TextInputType.phone : TextInputType.text,
    obscureText: isObscure,
    onChanged: onChange,
    enabled: isEnabled,
    textCapitalization: capitalization,
    onTap: onTap ?? () {},
    decoration: InputDecoration(
        helperText: "",
        hintText: placeholder,
        prefixIcon: Icon(prefixIcon),
        suffixIcon: suffixIcon,
        border: OutlineInputBorder(borderSide: BorderSide(width: 1))),
  );
}

import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';

class Dialogs {
  static alertDialog(BuildContext context, String text,
      {Function onDone, Function onAdd, String ok, String non}) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              text,
              style: TextStyle(
                fontFamily: "GoogleSans",
                fontSize: 15,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  ok,
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: "GoogleSans",
                    color: ThemeColor.mainColor,
                  ),
                ),
                onPressed: () {
                  onAdd();
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text(
                  non,
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: "GoogleSans",
                      color: Colors.black54),
                ),
                onPressed: () {
                  onDone();
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}

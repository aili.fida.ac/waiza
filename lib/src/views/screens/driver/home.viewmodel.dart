import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/models/request.model.dart';
import 'package:waiza/src/services/request.service.dart';

class DriverHomeViewModel extends BaseViewModel {
  bool _isLoading = false;
  RequestModel _requestModel;
  List _allData;

  bool get isLoading => _isLoading;
  RequestModel get requestModel => _requestModel;
  List get allData => _allData;

  void init(BuildContext context) async {
    try {
      _isLoading = true;
      notifyListeners();

      // Ovaina StreamBuilder snapshots
      var allData = await RequestService.getRequestAllWatting();
      _allData = allData.documents;
      print(_allData);
    } catch (e) {
      print("erreur ato am init DriverhomeView $e");
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }
}

import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/constants/message.dart';
import 'package:waiza/src/services/request.service.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/driver/home.dart';

class ClientRequestViewmodel extends BaseViewModel {
  bool _isLoading = false;
  bool get isLoading => _isLoading;

  void replyRequest(String id, BuildContext context) async {
    _isLoading = true;
    notifyListeners();
    try {
      var driver = await UserService.getUser();
      await RequestService.updateRequest(
        id,
        {
          'type': 1,
          'driver': {
            "id": driver.id,
            "name": driver.firstname,
            "phone": driver.phone
          }
        },
      );
      Toast.show("Demande accepté", context, duration: 3);
      Helpers.goto(context, DriverHomeView());
    } catch (e) {
      errorMessage(context);
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/driver/home.dart';
import 'package:waiza/src/views/screens/driver/requests/client.request.viewmodel.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class ClientRequest extends StatelessWidget {
  final String id, depart, destination;
  ClientRequest({this.id, this.depart, this.destination});
  @override
  Widget build(BuildContext context) {
    var markers = <Marker>[
      Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(51.5, -0.09),
        builder: (ctx) => MarkerDecoration("Client", Icons.my_location),
      ),
      Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(51.5, -0.087),
        builder: (ctx) => MarkerDecoration("Vous", Icons.local_taxi),
      ),
    ];
    return ViewModelBuilder<ClientRequestViewmodel>.reactive(
        builder: (context, model, _) => Scaffold(
              appBar: AppBar(
                iconTheme: IconThemeData(color: Colors.black),
                backgroundColor: Colors.transparent,
                elevation: 0,
              ),
              body: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  FlutterMap(
                    options: MapOptions(
                        center: LatLng(51.5, -0.09),
                        zoom: 15.0,
                        onTap: (position) {
                          print(position);
                        }),
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        subdomains: ['a', 'b', 'c'],
                        tileProvider: CachedNetworkTileProvider(),
                      ),
                      MarkerLayerOptions(markers: markers)
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * .3,
                    padding: EdgeInsets.all(24),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(24),
                        topRight: Radius.circular(24),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * .12,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        depart,
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    destination,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.attach_money,
                                    size: 32,
                                    color: ThemeColor.secondaryColor,
                                  ),
                                  Text(
                                    "20 000 Ar",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              child: Text(
                                "Ignorer",
                                style: TextStyle(color: Colors.grey),
                              ),
                              onPressed: () {
                                Helpers.goto(context, DriverHomeView());
                              },
                            ),
                            model.isLoading
                                ? Loader()
                                : RaisedButton(
                                    child: Text(
                                      "Repondre",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      model.replyRequest(id, context);
                                    },
                                    color: ThemeColor.mainColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    elevation: 0.0,
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
        viewModelBuilder: () => ClientRequestViewmodel());
  }
}

class MarkerDecoration extends StatelessWidget {
  final String role;
  final IconData icon;
  MarkerDecoration(this.role, this.icon);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 30,
            width: 80,
            margin: EdgeInsets.only(left: 20),
            decoration: BoxDecoration(
              color: ThemeColor.secondaryColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
                bottomRight: Radius.circular(16),
              ),
            ),
            child: Center(
              child: Text(
                role,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Icon(
            icon,
            size: 32,
            color: ThemeColor.secondaryColor,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';

class BottomSheetAction extends StatelessWidget {
  final title;
  final icon;
  final Widget screen;

  BottomSheetAction(this.title, this.icon, {this.screen});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Helpers.goto(context, screen),
      child: Container(
        height: 80,
        width: 80,
        margin: EdgeInsets.only(right: 24),
        child: Column(
          children: [
            Icon(
              icon,
              size: 48,
              color: ThemeColor.secondaryColor,
            ),
            Text(title)
          ],
        ),
      ),
    );
  }
}

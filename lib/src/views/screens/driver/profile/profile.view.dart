import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/views/screens/client/profile/profile.viewmodel.dart';
import 'package:waiza/src/views/screens/driver/profile/profile.viewmodel.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';

class DriverProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DriverProfileViewModel>.reactive(
        builder: (context, model, _) => SafeArea(
                child: Scaffold(
              body: Container(
                child: Column(
                  children: <Widget>[
                    Text(
                      "Complétez votre profil",
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    input("Nom",
                        prefixIcon: Icons.phone_iphone,
                        isPhone: true,
                        onChange: (text) {}),
                    SizedBox(
                      height: 16,
                    ),
                    input("Mot de passe",
                        prefixIcon: Icons.phone_iphone,
                        isPhone: true,
                        onChange: (text) {}),
                    SizedBox(
                      height: 16,
                    ),
                    input("Confirmation du mot de passe",
                        prefixIcon: Icons.lock,
                        isPhone: true,
                        onChange: (text) {}),
                    SizedBox(
                      height: 16,
                    ),
                    input("Prénom",
                        prefixIcon: Icons.lock,
                        isPhone: true,
                        onChange: (text) {}),
                    SizedBox(
                      height: 16,
                    ),
                    Center(
                      child: Text(
                        "Vous allez recevoir un code de validation par sms",
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    PrimaryButton(
                      "Valider",
                    ),
                  ],
                ),
              ),
            )),
        viewModelBuilder: () => DriverProfileViewModel());
  }
}

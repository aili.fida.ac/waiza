import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/clientProfil/clientProfil.view.dart';
import 'package:waiza/src/views/screens/client/finance/finance.view.dart';
import 'package:waiza/src/views/screens/driver/bottomSheetAction.dart';
import 'package:waiza/src/views/screens/driver/home.viewmodel.dart';
import 'package:waiza/src/views/screens/driver/requests/client.request.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class DriverHomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DriverHomeViewModel>.reactive(
      onModelReady: (model) => model.init(context),
      builder: (context, model, _) => SafeArea(
        child: Scaffold(
          backgroundColor: ThemeColor.mainColor,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.apps,
                  size: 32,
                  color: Colors.black,
                ),
                onPressed: () {
                  _settingModalBottomSheet(context);
                },
              ),
              SizedBox(
                width: 16,
              )
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Demande de Taxi",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .03),
                model.isLoading
                    ? Text("demandes ...")
                    : Text(
                        "${model.allData.length} demandes",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                SizedBox(height: MediaQuery.of(context).size.height * .06),
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                        child: model.isLoading
                            ? Center(child: Loader())
                            : model.allData.length == 0
                                ? Text("Pas encore de demande")
                                : Column(
                                    children: model.allData.map((e) {
                                    return RequestCard(
                                      id: e.documentID,
                                      depart: e['depart'],
                                      destination: e['destination'],
                                    );
                                  }).toList())),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      viewModelBuilder: () => DriverHomeViewModel(),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(16),
        topRight: Radius.circular(16),
      ),
    ),
    context: context,
    builder: (BuildContext bc) {
      return Container(
        height: MediaQuery.of(context).size.height * .23,
        padding: EdgeInsets.only(left: 32, right: 32, top: 16),
        child: Column(
          children: [
            Container(
              height: 6.0,
              width: 64.0,
              color: ThemeColor.secondaryColor,
            ),
            SizedBox(
              height: 32,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BottomSheetAction(
                    "Profil",
                    Icons.person_outline,
                    screen: ClientProfil(DriverHomeView()),
                  ),
                  // BottomSheetAction("Historiques", Icons.history),
                  BottomSheetAction(
                    "Finances",
                    Icons.attach_money,
                    screen: Finance(),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
  );
}

class RequestCard extends StatelessWidget {
  final String id, depart, destination;
  RequestCard({this.id, this.depart, this.destination});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 25),
      padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
      height: MediaQuery.of(context).size.height * .2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(24)),
        color: Colors.white,
      ),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * .18,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(16),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Container(
                        width: 2,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * .18,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * .4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * .4,
                          child: Text(
                            depart,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * .4,
                    child: Container(
                      width: MediaQuery.of(context).size.width * .3,
                      child: Text(
                        destination,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () {
                Helpers.goto(
                    context,
                    ClientRequest(
                      id: id,
                      depart: depart,
                      destination: destination,
                    ));
              },
              child: Container(
                height: MediaQuery.of(context).size.height * .1,
                width: MediaQuery.of(context).size.width * .16,
                child: Icon(
                  Icons.chevron_right,
                  size: 32,
                  color: Colors.white,
                ),
                decoration: BoxDecoration(
                  color: ThemeColor.secondaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/login/login.viewmodel.dart';
import 'package:waiza/src/views/screens/register/register.view.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
        builder: (context, model, _) => SafeArea(
              child: Scaffold(
                body: Container(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 48),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      input("Numéro de téléphone",
                          isPhone: true,
                          prefixIcon: Icons.phone_iphone,
                          onChange: (text) => model.setPhone(text)),
                      SizedBox(
                        height: 16,
                      ),
                      input("Mot de passe",
                          isObscure: model.pwdShow ? true : false,
                          prefixIcon: Icons.vpn_key,
                          suffixIcon: IconButton(
                            icon: Icon(model.pwdShow
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              model.setPwdShow();
                            },
                          ),
                          onChange: (text) => model.setPassword(text)),
                      SizedBox(
                        height: 32,
                      ),
                      model.isLoading
                          ? Loader()
                          : PrimaryButton("se connecter",
                              action: model.phone == "" || model.password == ""
                                  ? null
                                  : () {
                                      model.login(context);
                                    }),
                      FlatButton(
                          onPressed: () {
                            Helpers.goto(context, RegisterView());
                          },
                          child: Text(
                            "Je n'ai pas encore de compte",
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                decoration: TextDecoration.underline),
                          ))
                    ],
                  ),
                ),
              ),
            ),
        viewModelBuilder: () => LoginViewModel());
  }
}

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/services/auth.service.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';
import 'package:waiza/src/views/screens/driver/home.dart';

class LoginViewModel extends BaseViewModel {
  String _phone = "";
  String _password = "";
  bool _isLoading = false;
  bool _pwdShow = true;

  String get phone => _phone;
  String get password => _password;
  bool get isLoading => _isLoading;
  bool get pwdShow => _pwdShow;

  void setPhone(String text) {
    _phone = text;
    notifyListeners();
  }

  void setPassword(String text) {
    _password = text;
    notifyListeners();
  }

  void setPwdShow() {
    _pwdShow = !_pwdShow;
    notifyListeners();
  }

  void login(BuildContext context) {
    _isLoading = true;
    notifyListeners();
    AuthService.login(_phone, _password).then((value) {
      var data = {
        "id": value.documents[0].documentID,
        ...value.documents[0].data
      };

      value.documents.length > 0
          ? homeRedirection(context, UserModel.fromJson(data))
          : Toast.show(
              "Le numéro et le mot de passe ne correspondent pas.", context,
              duration: 3);
      _isLoading = false;
      notifyListeners();
    }).catchError((onError) {
      print("ato ndrai zan $onError");
      Toast.show(
          "Une erreur est survenue veuillez réessayer ultérieurement", context,
          duration: 3);
      _isLoading = false;
      notifyListeners();
    });
  }

  static void homeRedirection(BuildContext context, UserModel user) {
    Widget screen;
    UserService.setUser(user).then((value) {
      screen = user.userType == 0 ? ClientHomeView() : DriverHomeView();
      Helpers.goto(context, screen);
    });
  }
}

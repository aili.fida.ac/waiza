import 'package:flutter/material.dart';

class FinanceCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final style = TextStyle(color: Colors.grey, fontWeight: FontWeight.w400);
    return InkWell(
      onTap: () {},
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    image: DecorationImage(
                        image: AssetImage("images/rapper.jpg"),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Janny Michel",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      "Anosy",
                      style: style,
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      "Andoharanofotsy",
                      style: style,
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "1000 Ar",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.purple.withOpacity(0.8)),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Text(
                    "20 Juilet",
                    style: style,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

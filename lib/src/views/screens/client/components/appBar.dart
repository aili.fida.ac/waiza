import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';

AppBar myAppBar(String label, IconData icon) {
  return AppBar(
    elevation: 0,
    backgroundColor: ThemeColor.mainColor,
    iconTheme: IconThemeData(color: Colors.black),
    title: Text(
      label,
      style: TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
    ),
    centerTitle: true,
    actions: <Widget>[
      IconButton(icon: Icon(icon, color: Colors.black), onPressed: null)
    ],
  );
}

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class DriverRating extends StatelessWidget {
  final double rating;
  final int count;
  final bool border;
  DriverRating({this.rating, this.count, this.border});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        RatingBar(
          initialRating: rating,
          minRating: 0,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 4,
          itemSize: 24,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            border ? Icons.star_border : Icons.star,
            color: border ? Colors.black : Colors.amber,
          ),
          onRatingUpdate: null,
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          "$count",
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}

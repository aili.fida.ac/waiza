import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:stacked/stacked.dart';
import 'package:latlong/latlong.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/constants/message.dart';
import 'package:waiza/src/models/request.model.dart';
import 'package:waiza/src/services/location.service.dart';
import 'package:waiza/src/services/request.service.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';

class RequestTaxiViewModel extends BaseViewModel {
  LatLng _center = LatLng(0, 0);
  bool _showSearch = false;
  LatLng _destination = LatLng(0, 0);
  TextEditingController _controller = TextEditingController();
  List<LatLng> _points = [];
  MapController _mapController = MapController();
  bool _isLoading = false;
  RequestModel _requestModel;

  List<Marker> _markers = <Marker>[];

  List<Marker> get markers => _markers;
  LatLng get center => _center;
  bool get showSearch => _showSearch;
  List<LatLng> get points => _points;
  MapController get mapController => _mapController;
  TextEditingController get controller => _controller;
  bool get isLoading => _isLoading;
  RequestModel get requestModel => _requestModel;

  void init(BuildContext context) {
    LocationService.initLocationService(context).then((value) {
      _center = LatLng(value.latitude, value.longitude);
      _points.add(_center);
      _mapController.move(_center, 15.0);
      _markers.add(Marker(
        width: 118,
        height: 40,
        point: LatLng(value.latitude, value.longitude),
        builder: (ctx) => bubble(),
      ));
      notifyListeners();
    });
  }

  void onMarkerTap(LatLng pos) {
    if (_markers.length > 1) _markers.removeLast();
    _markers.add(_marker(pos));
    _destination = pos;
    _points.add(pos);
    notifyListeners();
  }

  void toggleSearchBox() {
    _showSearch = !_showSearch;
    notifyListeners();
  }

  void onDestinationTap() {
    _mapController.move(_destination, 15.0);
    notifyListeners();
  }

  void moveToPosition() {
    _mapController.move(_center, 15.0);
  }

  void moveToSearchPosition(LatLng points) {
    _mapController.move(points, 15.0);
    _showSearch = false;
    notifyListeners();
  }

  void sendRequest(BuildContext context) async {
    _isLoading = true;
    notifyListeners();
    try {
      var userPhone = await UserService.getUser();
      await RequestService.sendRequest({
        "customer": userPhone.phone,
        "depart": "Analakely",
        "destination": "Itaosy cité",
        "createdAt": DateTime.now(),
        "needAt": "9/08/2020",
        "type": 0
      });
      successMessage(context);
      Helpers.goto(context, ClientHomeView());
    } catch (e) {
      errorMessage(context);
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }
}

Marker _marker(LatLng position) {
  return Marker(
    width: 80.0,
    height: 80.0,
    point: position,
    builder: (ctx) => Bounce(
        infinite: true,
        child: Container(
            child: Icon(Icons.location_on, color: Colors.red, size: 44))),
  );
}

Widget bubble() {
  return Bounce(
    infinite: true,
    child: Stack(
      children: [
        Container(
            child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(.8),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Text(
            "Vous êtes ici",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        )),
        Positioned(
          bottom: -16,
          left: 38,
          child: Container(
            child: Icon(
              Icons.arrow_drop_down,
              color: Colors.black.withOpacity(.8),
              size: 42,
            ),
          ),
        )
      ],
    ),
  );
}

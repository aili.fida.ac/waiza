import 'package:address_search_field/address_search_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/requestTaxi/request.taxi.viewmodel.dart';
import 'package:waiza/src/views/screens/client/requestTaxi/target.information.view.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:latlong/latlong.dart';

class RequestTaxiView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RequestTaxiViewModel>.reactive(
        onModelReady: (model) {
          model.init(context);
        },
        builder: (context, model, child) {
          return SafeArea(
            child: Scaffold(
              body: Stack(
                children: [
                  FlutterMap(
                    mapController: model.mapController,
                    options: MapOptions(
                        center: model.center,
                        zoom: 5.0,
                        onTap: (position) {
                          model.onMarkerTap(position);
                        }),
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        subdomains: ['a', 'b', 'c'],
                        tileProvider: CachedNetworkTileProvider(),
                      ),
                      // PolylineLayerOptions(
                      //   polylines: [
                      //     Polyline(
                      //         points: model.points,
                      //         strokeWidth: 4.0,
                      //         color: Colors.purple),
                      //   ],
                      // ),
                      MarkerLayerOptions(markers: model.markers)
                    ],
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                        color: Colors.white.withOpacity(0.8),
                      ),
                      width: Helpers.deviceSize(context).width,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MutedButton(
                            "Voir ma position actuelle",
                            Icons.my_location,
                            action: () {
                              model.moveToPosition();
                            },
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          MutedButton(
                            "Voir ma destination",
                            Icons.location_on,
                            action: () {
                              model.onDestinationTap();
                            },
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          PrimaryButton("Prendre un taxi", action: () {
                            Helpers.goto(
                              context,
                              TargetInformationView(),
                            );
                          }),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    right: -10,
                    top: Helpers.deviceSize(context).height / 2,
                    child: FlatButton(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      color: Colors.black,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {
                        model.toggleSearchBox();
                      },
                      child: Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 32,
                      ),
                    ),
                  ),
                  model.showSearch
                      ? Positioned(
                          top: 8,
                          child: Opacity(
                            opacity: .7,
                            child: AddressSearchBox(
                              country: "Madagascar",
                              city: "Antananarivo",
                              hintText: "Destination",
                              coordForRef: true,
                              noResultsText: "Pas de resultats",
                              controller: TextEditingController(),
                              onDone: (BuildContext dialogContext,
                                  AddressPoint point) {
                                model.moveToSearchPosition(
                                    LatLng(point.latitude, point.longitude));
                              },
                              onCleaned: () {},
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          );
        },
        viewModelBuilder: () => RequestTaxiViewModel());
  }
}

Widget bubble() {
  return Stack(
    children: [
      Container(
          child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(.8),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text("Vous êtes ici"),
      )),
      Positioned(
        bottom: -20,
        left: 38,
        child: Container(
          child: Icon(
            Icons.arrow_drop_down,
            color: Colors.white.withOpacity(.8),
            size: 42,
          ),
        ),
      )
    ],
  );
}

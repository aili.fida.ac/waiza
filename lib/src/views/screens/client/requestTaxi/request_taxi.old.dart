import 'package:address_search_field/address_search_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';

class RequestTaxiOldView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: ThemeColor.mainColor,
        appBar: AppBar(
          backgroundColor: ThemeColor.mainColor,
          elevation: 0,
        ),
        body: Container(
            margin: EdgeInsets.only(top: 16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: DefaultTabController(
              length: 2,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 24, vertical: 48),
                child: Column(
                  children: [
                    TabBar(
                      isScrollable: true,
                      tabs: [
                        Tab(
                          child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 24),
                              child: Text("Tout de suite")),
                        ),
                        Tab(
                          child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 24),
                              child: Text("Planifier")),
                        )
                      ],
                      indicatorColor: Colors.black,
                      labelColor: Colors.black,
                    ),
                    Expanded(child: TabBarView(children: [Now(), Planned()]))
                  ],
                ),
              ),
            )));
  }
}

class Now extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 32,
          ),
          input("Destination"),
          SizedBox(
            height: 16,
          ),
          input("Nombre de personnes", isPhone: true),
          SizedBox(
            height: 48,
          ),
          Bagages(),
          Spacer(),
          SecondaryButton(
            "Envoyer",
            action: () {
              Helpers.goto(context, ClientHomeView());
            },
          )
        ],
      ),
    );
  }
}

class Planned extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 32,
          ),
          input("Date et heure du voyage"),
          SizedBox(
            height: 16,
          ),
          Container(
            width: double.infinity,
            child: AddressSearchBox(
              country: "Madagascar",
              city: "Antananarivo",
              hintText: "Destination",
              coordForRef: true,
              noResultsText: "Pas de resultats",
              controller: TextEditingController(),
              onDone: (BuildContext dialogContext, AddressPoint point) {},
              onCleaned: () {},
            ),
          ),
          SizedBox(
            height: 16,
          ),
          input("Nombre de personnes"),
          SizedBox(
            height: 48,
          ),
          Bagages(),
          Spacer(),
          SecondaryButton(
            "Envoyer",
            action: () {
              Helpers.goto(context, ClientHomeView());
            },
          )
        ],
      ),
    );
  }
}

class Bagages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Bagages",
                style: Theme.of(context).textTheme.headline6,
              ),
              Container(
                child: Transform.scale(
                  scale: .7,
                  child: CupertinoSwitch(
                    value: true,
                    onChanged: (value) {},
                    activeColor: ThemeColor.mainColor,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

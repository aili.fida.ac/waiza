import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/views/screens/client/profile/profile.viewmodel.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class ClientProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProfileViewModel>.reactive(
        builder: (context, model, _) => SafeArea(
                child: Scaffold(
              body: Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 48),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Complétez votre profil",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      input("Nom",
                          capitalization: TextCapitalization.characters,
                          prefixIcon: Icons.phone_iphone, onChange: (text) {
                        model.setInput(text, 0);
                      }),
                      SizedBox(
                        height: 16,
                      ),
                      input("Prénom",
                          prefixIcon: Icons.lock,
                          capitalization: TextCapitalization.words,
                          onChange: (text) {
                        model.setInput(text, 1);
                      }),
                      SizedBox(
                        height: 16,
                      ),
                      input("Mot de passe",
                          isObscure: model.pwdShow ? true : false,
                          prefixIcon: Icons.phone_iphone,
                          suffixIcon: IconButton(
                            icon: Icon(model.pwdShow
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              model.setPwdShow();
                            },
                          ), onChange: (text) {
                        model.setInput(text, 2);
                      }),
                      SizedBox(
                        height: 16,
                      ),
                      input("Confirmation du mot de passe",
                          prefixIcon: Icons.lock,
                          suffixIcon: IconButton(
                            icon: Icon(model.pwdShow
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              model.setPwdShow();
                            },
                          ),
                          isObscure: model.pwdShow ? true : false,
                          onChange: (text) {
                        model.setInput(text, 3);
                      }),
                      model.password != model.confirmationPassword
                          ? Text(
                              "Les Mots de passe doivent être identique",
                              style: TextStyle(color: Colors.red),
                            )
                          : Text(""),
                      SizedBox(
                        height: 16,
                      ),
                      model.isLoading
                          ? Loader()
                          : PrimaryButton(
                              "Valider",
                              action: model.firstname == "" ||
                                      model.lastname == "" ||
                                      model.password == "" ||
                                      model.password !=
                                          model.confirmationPassword
                                  ? null
                                  : () {
                                      model.registerClient(context);
                                    },
                            ),
                    ],
                  ),
                ),
              ),
            )),
        viewModelBuilder: () => ProfileViewModel());
  }
}

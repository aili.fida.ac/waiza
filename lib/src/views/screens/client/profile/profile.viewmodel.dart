import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/services/auth.service.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';

class ProfileViewModel extends BaseViewModel {
  UserModel _user;

  String _firstname, _lastname, _password, _confirmationPassword = "";
  bool _isValid = false;
  bool _pwdShow = true;
  bool _isLoading = false;

  String get firstname => _firstname;
  String get lastname => _lastname;
  String get password => _password;
  bool get isValid => _isValid;
  String get confirmationPassword => _confirmationPassword;
  bool get pwdShow => _pwdShow;
  bool get isLoading => _isLoading;
  UserModel get user => _user;

  void setInput(String text, int cas) {
    switch (cas) {
      case 0:
        _firstname = text;
        break;
      case 1:
        _lastname = text;
        break;
      case 2:
        _password = text;
        break;
      case 3:
        _confirmationPassword = text;
        break;
      default:
        _confirmationPassword = text;
        break;
    }

    notifyListeners();
  }

  void setPwdShow() {
    _pwdShow = !_pwdShow;
    notifyListeners();
  }

  void registerClient(BuildContext context) {
    _isLoading = true;
    notifyListeners();
    UserService.getPhoneStorage().then((phone) {
      AuthService.register(0, {
        "firstname": _firstname,
        "lastname": _lastname,
        "password": _confirmationPassword,
        "phone": phone,
        "userType": 0
      }).then((value) {
        UserService.setUser(UserModel.fromJson({
          "id": value.documentID,
          "firstname": _firstname,
          "lastname": _lastname,
          "password": _password,
          "phone": phone,
          "userType": 0
        })).then((value) {
          Helpers.goto(context, ClientHomeView());
        });
        _isLoading = true;
        notifyListeners();
        Toast.show("Bienvenue dans Waiza", context);
      }).catchError((onError) {
        print(onError);
        Toast.show("Something went wrong, please retry.", context);
        _isLoading = false;
        notifyListeners();
      });
    });
  }
}

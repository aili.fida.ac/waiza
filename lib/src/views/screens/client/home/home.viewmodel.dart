import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/models/request.model.dart';
import 'package:waiza/src/services/request.service.dart';
import 'package:waiza/src/services/user.service.dart';

class HomeViewModel extends BaseViewModel {
  bool _isLoading = false;
  RequestModel _requestModel;
  List _allData, _reqAccepted;
  String _firstData;

  bool get isLoading => _isLoading;
  RequestModel get requestModel => _requestModel;
  List get allData => _allData;
  String get firstData => _firstData;
  List get reqAccepted => _reqAccepted;

  void init(BuildContext context) async {
    try {
      _isLoading = true;
      notifyListeners();
      var userPhone = await UserService.getUser();
      var reqWatting =
          await RequestService.getRequestUserWatting(userPhone.phone);

      // Ovaina StreamBuilder snapshots
      var reqAccepted =
          await RequestService.getRequestUserAccepted(userPhone.phone);
      if (reqAccepted.documents.length == 0) {
        _reqAccepted = [];
      } else {
        _reqAccepted = reqAccepted.documents;
      }

      _allData = reqWatting.documents;
      _firstData = reqWatting.documents[0].data['needAt'];
    } catch (e) {
      print("erreur ato am init homeView $e");
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }
}

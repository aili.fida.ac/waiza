import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/clientProfil/clientProfil.view.dart';
import 'package:waiza/src/views/screens/client/deplacement/deplacement.view.dart';
import 'package:waiza/src/views/screens/client/driverProfile/driver.client.dart';
import 'package:waiza/src/views/screens/client/finance/finance.view.dart';
import 'package:waiza/src/views/screens/client/home/bottomSheetAction.dart';
import 'package:waiza/src/views/screens/client/home/eventCard.dart';
import 'package:waiza/src/views/screens/client/home/home.viewmodel.dart';
import 'package:waiza/src/views/screens/client/notification/notification.view.dart';
import 'package:waiza/src/views/screens/client/requestTaxi/request_taxi.dart';
import 'package:waiza/src/views/screens/client/responseTaxi/detail.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class ClientHomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 10),
          decoration: BoxDecoration(
            color: ThemeColor.mainColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Header(),
              SizedBox(
                height: MediaQuery.of(context).size.height * .005,
              ),
              Dashboard(),
              PlannedSection(),
              NotificationSection(),
              SecondaryButton("Besoin d'un taxi ?",
                  action: () => Helpers.goto(context, RequestTaxiView()))
            ],
          ),
        ),
      ),
    );
  }
}

class NotificationSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
        onModelReady: (model) => model.init(context),
        builder: (context, model, _) => SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 3,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Notifications",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      InkWell(
                        child: Text("Voir tout"),
                        onTap: () {
                          Helpers.goto(context, NotificationView());
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * .3,
                    child: model.isLoading
                        ? Loader()
                        : model.reqAccepted.length == 0
                            ? Text("Pas encore de demande accepté")
                            : SingleChildScrollView(
                                child: Column(
                                  children: model.reqAccepted.map((e) {
                                    print(e['type']);
                                    return EventCard(
                                      "Janny Michel a répondu à\nvotre requete.",
                                      "Voir",
                                      onTap: () =>
                                          Helpers.goto(context, Detail()),
                                    );
                                  }).toList(),
                                ),
                              ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
        viewModelBuilder: () => HomeViewModel());
  }
}

class PlannedSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
        onModelReady: (model) => model.init(context),
        builder: (context, model, _) => Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Déplacement planifié",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .05,
                    ),
                    InkWell(
                      child: Text("Voir tout"),
                      onTap: () {
                        Helpers.goto(context, Deplacement());
                      },
                    ),
                  ],
                ),
                model.isLoading
                    ? Text("...")
                    : EventCard(
                        "Vous avez un déplacement\nplanifieé le ${model.firstData}",
                        "Voir le détail",
                        onTap: () {
                          Helpers.goto(context, Deplacement());
                        },
                      ),
              ],
            ),
        viewModelBuilder: () => HomeViewModel());
  }
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .25,
      decoration: BoxDecoration(
        color: ThemeColor.secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(24)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * .08,
                  width: MediaQuery.of(context).size.width * .12,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  child: Icon(
                    Icons.local_taxi,
                    color: ThemeColor.secondaryColor,
                    size: 32,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * .08,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "10",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "déplacements",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 12,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "Total",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * .1,
                ),
                Text(
                  "100 000 Ar",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .04,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "WAIZA",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
          InkWell(
            onTap: () {
              _settingModalBottomSheet(context);
            },
            child: Icon(
              Icons.apps,
              size: 32,
            ),
          ),
        ],
      ),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(16),
        topRight: Radius.circular(16),
      ),
    ),
    context: context,
    builder: (BuildContext bc) {
      return Container(
        height: MediaQuery.of(context).size.height * .25,
        padding: EdgeInsets.only(left: 32, right: 32, top: 16),
        child: Column(
          children: [
            Container(
              height: 6.0,
              width: 64.0,
              color: ThemeColor.secondaryColor,
            ),
            SizedBox(
              height: 32,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BottomSheetAction(
                    "Profil",
                    Icons.person_outline,
                    screen: ClientProfil(ClientHomeView()),
                  ),
                  BottomSheetAction(
                    "Notification",
                    Icons.notifications_none,
                    screen: NotificationView(),
                  ),
                  // BottomSheetAction("Historiques", Icons.history),
                  BottomSheetAction(
                    "Finances",
                    Icons.attach_money,
                    screen: Finance(),
                  ),
                  BottomSheetAction(
                    "Coursiers",
                    Icons.local_taxi,
                    screen: Driver(),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
  );
}

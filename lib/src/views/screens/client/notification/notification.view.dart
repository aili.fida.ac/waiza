import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/components/cardTile.dart';
import 'package:waiza/src/views/screens/client/home/home.viewmodel.dart';
import 'package:waiza/src/views/screens/client/responseTaxi/detail.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class NotificationView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.orangeAccent,
          appBar: _appBar(),
          body: _body(size, context)),
    );
  }

  Widget _body(Size size, BuildContext context) {
    final radius = BorderRadius.only(
        topLeft: Radius.circular(35.0), topRight: Radius.circular(35.0));
    return Container(
      width: double.infinity,
      height: Helpers.deviceSize(context).height,
      decoration: BoxDecoration(color: Colors.white, borderRadius: radius),
      child: ClipRRect(
        borderRadius: radius,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              NotifRecent(isRecente: true),
              NotifRecent(isRecente: false)
            ],
          ),
        ),
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      elevation: 0,
      iconTheme: IconThemeData(color: Colors.black),
      backgroundColor: Colors.orangeAccent,
      title: Text(
        "Notification",
        style: TextStyle(color: Colors.black),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.notifications_none,
            color: Colors.black,
          ),
          onPressed: null,
        )
      ],
    );
  }
}

class NotifRecent extends StatelessWidget {
  final bool isRecente;
  NotifRecent({this.isRecente});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      onModelReady: (model) => model.init(context),
      builder: (context, model, _) => Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  model.isLoading
                      ? Text("Toutes ...")
                      : isRecente
                          ? Text(
                              "Recente (${model.reqAccepted.length})",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18.0),
                            )
                          : Text(
                              "Toutes (${model.reqAccepted.length})",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18.0),
                            ),
                ],
              ),
            ),
            model.isLoading
                ? Loader()
                : ListView.builder(
                    itemCount: model.reqAccepted.length,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return CardTile(
                        action: () {
                          Helpers.goto(context, Detail());
                        },
                      );
                    }),
          ],
        ),
      ),
      viewModelBuilder: () => HomeViewModel(),
    );
  }
}

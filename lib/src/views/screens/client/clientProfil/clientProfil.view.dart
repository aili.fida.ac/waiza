import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/clientProfil/clientProfil.viewmodel.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class ClientProfil extends StatelessWidget {
  final Widget screenBack;
  ClientProfil(this.screenBack);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ClientProfileViewModel>.reactive(
        onModelReady: (model) => model.init(),
        builder: (context, model, _) => SafeArea(
              child: Scaffold(
                body: Container(
                    child: Column(
                  children: <Widget>[
                    Container(
                      color: ThemeColor.mainColor,
                      padding: EdgeInsets.only(top: 25),
                      height: Helpers.deviceSize(context).height * 0.45,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                onPressed: () => model.logout(context),
                                icon: Icon(
                                  Icons.power_settings_new,
                                  size: 30,
                                ),
                              ),
                              IconButton(
                                onPressed: () =>
                                    Helpers.goto(context, screenBack),
                                icon: Icon(
                                  Icons.close,
                                  size: 30,
                                ),
                              )
                            ],
                          ),
                          Stack(
                            children: <Widget>[
                              CircleAvatar(
                                backgroundColor: Colors.white,
                                radius: 50,
                                backgroundImage:
                                    AssetImage("images/user_1.jpg"),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: CircleAvatar(
                                    radius: 15,
                                    backgroundColor: ThemeColor.greyColor,
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.black,
                                      size: 20,
                                    ),
                                  ))
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          model.isLoading
                              ? Loader()
                              : Text(
                                  "${model.user.firstname} ${model.user.lastname}",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                          model.isLoading
                              ? Text("")
                              : Text(model.user.firstname)
                        ],
                      ),
                    ),
                    Container(
                      color: ThemeColor.mainColor,
                      child: Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.only(top: 25.0, left: 25.0, right: 25),
                        height: Helpers.deviceSize(context).height * 0.5,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25),
                          ),
                        ),
                        child: Container(
                            child: SingleChildScrollView(
                          child: model.isLoading
                              ? Loader()
                              : Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        ProfilBody(
                                          "Nom d'utilisateur",
                                          "${model.user.firstname} ${model.user.lastname}",
                                          Icons.person,
                                        ),
                                        SizedBox(
                                          height: 25,
                                        ),
                                        ProfilBody("Téléphone",
                                            model.user.firstname, Icons.phone),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 25,
                                    ),
                                    PrimaryButton("modifier", action: () {})
                                  ],
                                ),
                        )),
                      ),
                    )
                  ],
                )),
              ),
            ),
        viewModelBuilder: () => ClientProfileViewModel());
  }
}

class ProfilBody extends StatelessWidget {
  final String title, text;
  final IconData icon;
  ProfilBody(this.title, this.text, this.icon);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            border: Border.all(
              color: ThemeColor.secondaryColor,
              width: 2.0,
            ),
          ),
          child: Icon(
            icon,
            size: 30,
            color: ThemeColor.secondaryColor,
          ),
        ),
        SizedBox(
          width: 25,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              this.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              this.text,
              style: TextStyle(color: ThemeColor.greyColor),
            )
          ],
        )
      ],
    );
  }
}

import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/widgets/dialog.dart';

class ClientProfileViewModel extends BaseViewModel {
  UserModel _user;
  UserModel get user => _user;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  void init() {
    _isLoading = true;
    notifyListeners();
    UserService.getUser().then((value) {
      _user = value;
      _isLoading = false;
      notifyListeners();
    });
  }

  void logout(BuildContext context) {
    Dialogs.alertDialog(context, "Voulez-vous vous déconnecter?",
        ok: "Oui", non: "Non", onAdd: () {
      UserService.logout(context);
    }, onDone: () {});
  }
}

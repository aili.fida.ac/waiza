import 'package:flutter/material.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/views/screens/client/components/appBar.dart';
import 'package:waiza/src/views/screens/client/components/financeCard.dart';

class Finance extends StatefulWidget {
  @override
  _FinanceState createState() => _FinanceState();
}

class _FinanceState extends State<Finance> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: ThemeColor.mainColor,
      appBar: myAppBar("Finance", Icons.account_balance_wallet),
      body: Column(
        children: <Widget>[
          _cardMoneySpended(size),
          _body(size),
        ],
      ),
    ));
  }

  Container _cardMoneySpended(Size size) {
    return Container(
      padding: EdgeInsets.all(16.0),
      height: size.height * 0.35,
      width: double.infinity,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: size.height * 0.4 / 3,
                  width: size.width * 0.4,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.6),
                    borderRadius: BorderRadius.all(Radius.circular(16.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Mois",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w300),
                            ),
                            Text(
                              "JUIL",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Colors.white),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios, color: Colors.white),
                    SizedBox(width: 16.0),
                    Icon(Icons.arrow_forward_ios, color: Colors.white)
                  ],
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _moneySpended("50 000 Ar", "Cette semaine"),
                _moneySpended("150 000 Ar", "Ce mois"),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _moneySpended(String money, String label) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          money,
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: 4.0,
        ),
        Text(
          label,
          style: TextStyle(color: Colors.white),
        )
      ],
    );
  }

  Widget _body(Size size) {
    final radius = BorderRadius.only(
        topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0));
    return Expanded(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white, borderRadius: radius),
        child: ClipRRect(
          borderRadius: radius,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    top: 16.0, bottom: 8.0, left: 16.0, right: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Détails (3)",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: 5,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: FinanceCard(),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

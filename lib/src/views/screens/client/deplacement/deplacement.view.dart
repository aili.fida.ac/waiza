import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/views/screens/client/components/appBar.dart';
import 'package:waiza/src/views/screens/client/components/cardTile.dart';
import 'package:waiza/src/views/screens/client/home/home.viewmodel.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class Deplacement extends StatefulWidget {
  @override
  _DeplacementState createState() => _DeplacementState();
}

class _DeplacementState extends State<Deplacement> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: ThemeColor.mainColor,
      appBar: myAppBar("Déplacement planifé", Icons.history),
      body: Column(
        children: <Widget>[
          _cardSpendedMoney(size),
          Body(size),
        ],
      ),
    ));
  }

  Container _cardSpendedMoney(Size size) {
    return Container(
      padding: EdgeInsets.all(8.0),
      height: size.height * 0.37,
      width: double.infinity,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(
              Radius.circular(24.0),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: size.width * .4,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.6),
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "15\nJuil",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        Container(
                          height: 50,
                          width: 1,
                          color: Colors.white,
                        ),
                        Column(
                          children: <Widget>[
                            Icon(Icons.timer, color: Colors.white),
                            Text(
                              "8:30",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                _button("Méteo", Icons.my_location, null),
                _button("BNI Analakely", Icons.location_on, null),
                Container(
                  margin: EdgeInsets.only(left: 8.0, top: 4.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Réporter",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(width: 16.0),
                      Text(
                        "Annuler",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  height: size.height * 0.4 / 3,
                  width: size.width * 0.3,
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      image: DecorationImage(
                          image: AssetImage("images/rapper.jpg"),
                          fit: BoxFit.cover)),
                ),
                SizedBox(height: 8.0),
                Text(
                  "Janny Michel",
                  style: TextStyle(
                      color: Colors.white38,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                SizedBox(height: 8.0),
                Row(
                  children: <Widget>[
                    Text("5",
                        style: TextStyle(color: Colors.white24, fontSize: 18)),
                    SizedBox(width: 8.0),
                    Icon(Icons.star_border, color: Colors.white24)
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _button(String label, IconData icon, Function action) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: Colors.white38,
          ),
          SizedBox(width: 4.0),
          Text(
            label,
            style: TextStyle(color: Colors.white38),
          )
        ],
      ),
    );
  }
}

class Body extends StatelessWidget {
  Size size;
  Body(this.size);
  @override
  Widget build(BuildContext context) {
    final radius = BorderRadius.only(
        topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0));
    return ViewModelBuilder<HomeViewModel>.reactive(
        onModelReady: (model) => model.init(context),
        builder: (context, model, _) => Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: radius),
                child: ClipRRect(
                  borderRadius: radius,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            model.isLoading
                                ? Text("Récentes ..")
                                : Text("Récentes (${model.reqAccepted.length})",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0)),
                          ],
                        ),
                      ),
                      model.isLoading
                          ? Loader()
                          : Expanded(
                              child: ListView.builder(
                                itemCount: model.reqAccepted.length,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    child: CardTile(action: null),
                                  );
                                },
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
        viewModelBuilder: () => HomeViewModel());
  }
}

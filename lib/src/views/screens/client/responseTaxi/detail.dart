import 'package:flutter/material.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/client/components/rating.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/map.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: <Widget>[_header(size), _body(size)],
        ),
      ),
    );
  }

  Widget _body(Size size) {
    return Container(
      margin: EdgeInsets.only(top: size.height * 0.5),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(35.0), topRight: Radius.circular(35.0)),
      ),
      child: Padding(
        padding: EdgeInsets.all(24.0),
        child: _aboutDriver(),
      ),
    );
  }

  Widget _aboutDriver() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Janny Michel",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  DriverRating(
                    rating: 1.2,
                    count: 5,
                    border: false,
                  ),
                ],
              ),
              Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    image: DecorationImage(
                        image: AssetImage("images/driver.jpg"),
                        fit: BoxFit.cover)),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Text(
            "Vehicule",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Renault R12"),
              FlatButton.icon(
                  onPressed: () {},
                  icon: Icon(
                    Icons.place,
                    color: Colors.black,
                  ),
                  label: Text(
                    "à 5m de vous",
                    style: TextStyle(color: Colors.black),
                  ))
            ],
          ),
          PrimaryButton(
            "Je le prends",
            action: () {
              Helpers.goto(context, ClientHomeView());
            },
          )
        ],
      ),
    );
  }

  Widget _header(Size size) {
    return Container(
      width: double.infinity,
      height: size.height * 0.6,
      child: Stack(
        children: <Widget>[
          PolylinePage(),
          SafeArea(child: BackButton()),
        ],
      ),
    );
  }
}

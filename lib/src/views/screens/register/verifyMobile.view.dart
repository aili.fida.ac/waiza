import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/views/screens/register/register.viewmodel.dart';
import 'package:waiza/src/views/widgets/button.dart';

class VerifyMobileView extends StatelessWidget {
  final String code;
  VerifyMobileView(this.code);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RegisterViewModel>.reactive(
        builder: (context, model, child) => SafeArea(
                child: Scaffold(
              body: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 48),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Spacer(),
                      Text(
                        "Veuillez entrer le code de validation",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      VerificationCode(
                        textStyle:
                            TextStyle(fontSize: 20.0, color: Colors.black),
                        keyboardType: TextInputType.number,
                        length: 6,
                        onCompleted: (String value) {
                          model.setCode(value);
                        },
                        onEditing: (bool value) => model.setEdition(value),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            "Renvoyer le code",
                            style: TextStyle(color: Colors.blue, fontSize: 16),
                          )),
                      Spacer(),
                      PrimaryButton(
                        "Valider",
                        action: () {
                          model.validate(context, code);
                        },
                      )
                    ],
                  )),
            )),
        viewModelBuilder: () => RegisterViewModel());
  }
}

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/constants/message.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/services/auth.service.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/register/register.info.view.dart';

class RegisterViewModel extends BaseViewModel {
  String _phone = "";
  String _code = "";
  bool _isEditing = true;
  bool _isLoading = false;

  String get phone => _phone;
  String get code => _code;
  bool get isEditing => _isEditing;
  bool get isLoading => _isLoading;

  void setPhone(String text) {
    _phone = text;
    notifyListeners();
  }

  void setEdition(bool edition) {
    _isEditing = edition;
    notifyListeners();
  }

  void setCode(String text) {
    _code = text;
    print(text);
    notifyListeners();
  }

  void sendCodeToPhone(BuildContext context) async {
    _isLoading = true;
    notifyListeners();
    try {
      var ifexist = await AuthService.checkIfExiste(_phone);
      if (ifexist.documents.length != 0) {
        userExist(context);
      } else {
        AuthService.sendCodeToPhoneNumber(context, _phone);
      }
    } catch (e) {
      errorMessage(context);
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }

  void setPhoneStorage() {
    UserService.setPhoneStorage(_phone).then((value) {
      print("phone sent success to storage");
    }).catchError((onError) {
      print("Error $onError");
    });
  }

  void validate(BuildContext context, String verificationID) {
    AuthService.validation(context, code, verificationID)
        .then((value) => Helpers.goto(context, RegisterInfoView()));
  }

  void testDriver() {
    UserService.setUser(UserModel.fromJson({
      "id": "121222ddzfs",
      "firstname": "Soa",
      "lastname": "Soa",
      "password": "ge51dfg",
      "phone": "+261324625734",
      "address": "Tanana",
      "username": "Rakoto",
      "userType": 1
    })).then((value) {
      print("vita");
    });
  }
}

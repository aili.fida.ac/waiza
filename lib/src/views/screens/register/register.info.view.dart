import 'package:flutter/material.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/client/profile/profile.view.dart';
import 'package:waiza/src/views/screens/driver/home.dart';

class RegisterInfoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 48, bottom: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Sélectionnez un profil",
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(fontWeight: FontWeight.w500, color: Colors.black),
              ),
              SizedBox(
                height: 48,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    profileCard(context, "Client", "images/client.jpg",
                        action: () {
                      Helpers.goto(context, ClientProfileView(),
                          isReplaced: true);
                    }),
                    SizedBox(
                      width: 16,
                    ),
                    profileCard(context, "Conducteur", "images/driver.jpg",
                        action: () {
                      UserService.setUser(UserModel.fromJson({
                        "id": "121222ddzfs",
                        "firstname": "Soa",
                        "lastname": "Soa",
                        "password": "ge51dfg",
                        "phone": "+261324625734",
                        "address": "Tanana",
                        "username": "Rakoto",
                        "userType": 1
                      })).then((value) {
                        print("vitaaaaaaa");
                      });
                      Helpers.goto(context, DriverHomeView(), isReplaced: true);
                    }),
                  ],
                ),
              ),
              Spacer(),
              Text("Lorem ")
            ],
          ),
        ),
      ),
    );
  }
}

Widget profileCard(BuildContext context, String title, String image,
    {Function action}) {
  return InkWell(
    onTap: action,
    child: Container(
      width: Helpers.deviceSize(context).width * .6,
      height: Helpers.deviceSize(context).height * .5,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(.5), BlendMode.overlay),
              fit: BoxFit.cover,
              image: AssetImage(image))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 24),
          ),
          SizedBox(height: 8),
          Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis at in facilisi amet at etiam.",
            style: TextStyle(color: Colors.white),
          )
        ],
      ),
    ),
  );
}

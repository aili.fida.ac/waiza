import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/login/login.view.dart';
import 'package:waiza/src/views/screens/register/register.viewmodel.dart';
import 'package:waiza/src/views/widgets/button.dart';
import 'package:waiza/src/views/widgets/input.dart';
import 'package:waiza/src/views/widgets/loader.dart';

class RegisterView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RegisterViewModel>.reactive(
        builder: (context, model, child) => SafeArea(
              child: Scaffold(
                body: Container(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 48),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Veuillez entrer votre numéro de téléphone",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      input("Téléphone",
                          prefixIcon: Icons.phone_iphone,
                          isPhone: true, onChange: (text) {
                        model.setPhone(text);
                      }),
                      SizedBox(
                        height: 16,
                      ),
                      Center(
                        child: Text(
                          "Vous allez recevoir un code de validation par sms",
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      model.isLoading
                          ? Center(
                              child: Loader(),
                            )
                          : PrimaryButton(
                              "Suivant",
                              action: model.phone == ""
                                  ? null
                                  : () {
                                      model.sendCodeToPhone(context);
                                      model.setPhoneStorage();
                                    },
                            ),
                      Center(
                        child: FlatButton(
                            onPressed: () {
                              Helpers.goto(context, LoginView());
                            },
                            child: Text(
                              "J'ai déjà un compte",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  decoration: TextDecoration.underline),
                            )),
                      )
                    ],
                  ),
                ),
              ),
            ),
        viewModelBuilder: () => RegisterViewModel());
  }
}

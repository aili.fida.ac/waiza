import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/colors.dart';
import 'package:waiza/src/views/screens/splash/splash.viewmodel.dart';

class SlpashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SplashViewModel>.reactive(
        onModelReady: (model) =>
            Timer(Duration(seconds: 2), () => model.init(context)),
        builder: (context, model, _) => SafeArea(
              child: Scaffold(
                backgroundColor: ThemeColor.mainColor,
                body: Center(
                  child: Text(
                    "WAIZA",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 50,
                    ),
                  ),
                ),
              ),
            ),
        viewModelBuilder: () => SplashViewModel());
  }
}

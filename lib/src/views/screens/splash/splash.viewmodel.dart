import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/services/user.service.dart';
import 'package:waiza/src/views/screens/client/home/home.view.dart';
import 'package:waiza/src/views/screens/driver/home.dart';
import 'package:waiza/src/views/screens/login/login.view.dart';

class SplashViewModel extends BaseViewModel {
  void init(BuildContext context) {
    UserService.getUser().then((value) {
      print("mande ato");
      print(value);
      value != null
          ? Helpers.goto(context,
              value.userType == 0 ? ClientHomeView() : DriverHomeView())
          : Helpers.goto(context, LoginView());
    });
  }
}

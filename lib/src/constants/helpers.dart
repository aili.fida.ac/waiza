import 'package:flutter/material.dart';
import 'package:waiza/src/constants/transitions.dart';

class Helpers {
  static Size deviceSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static void goto(BuildContext context, Widget screen,
      {bool isReplaced: false, bool isHorizontal = true}) {
    isReplaced
        ? Navigator.of(context).pushReplacement(
            Transitions.sharedAxisPageTransition(screen, isHorizontal))
        : Navigator.of(context)
            .push(Transitions.sharedAxisPageTransition(screen, isHorizontal));
  }
}

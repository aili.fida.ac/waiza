import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';

void errorMessage(BuildContext context) {
  Toast.show(
      "Une erreur est survenue veuillez réessayer ultérieurement", context,
      duration: 3);
}

void successMessage(BuildContext context) {
  Toast.show("Votre demande a été enregister", context, duration: 3);
}

void userExist(BuildContext context) {
  Toast.show("Utilisateur existant", context, duration: 3);
}

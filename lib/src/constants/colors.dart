import 'package:flutter/material.dart';

class ThemeColor {
  static final Color mainColor = Color.fromRGBO(255, 187, 55, 1);
  static final Color secondaryColor = Color.fromRGBO(117, 91, 150, 1);
  static final Color greyColor = Color.fromRGBO(196, 196, 196, 1);
}

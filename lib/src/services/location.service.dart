import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';

class LocationService {
  static Future<LocationData> initLocationService(BuildContext context) async {
    final Location _locationService = Location();
    bool _permission = false;
    LocationData location;
    bool serviceEnabled;
    bool serviceRequestResult;

    await _locationService.changeSettings(
      accuracy: LocationAccuracy.high,
      interval: 1000,
    );

    try {
      serviceEnabled = await _locationService.serviceEnabled();

      if (serviceEnabled) {
        var permission = await _locationService.requestPermission();
        _permission = permission == PermissionStatus.granted;

        if (_permission) {
          location = await _locationService.getLocation();
          return location;
        }
      } else {
        serviceRequestResult = await _locationService.requestService();
        if (serviceRequestResult) {
          initLocationService(context);
        }
      }
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        Toast.show(e.message, context);
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        Toast.show(e.message, context);
      }
      location = null;
    }
  }
}

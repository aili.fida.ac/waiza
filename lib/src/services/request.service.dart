import 'package:cloud_firestore/cloud_firestore.dart';

class RequestService {
  static Future<DocumentReference> sendRequest(dynamic data) async {
    return await Firestore.instance.collection("request").add(data);
  }

  static Future<QuerySnapshot> getRequestUserWatting(String userPhone) async {
    return await Firestore.instance
        .collection("request")
        .orderBy("createdAt", descending: true)
        .where("customer", isEqualTo: userPhone)
        .where("type", isEqualTo: 0)
        .getDocuments();
  }

  static Future<QuerySnapshot> getRequestUserAccepted(String userPhone) async {
    return await Firestore.instance
        .collection("request")
        .orderBy("createdAt", descending: true)
        .where("customer", isEqualTo: userPhone)
        .where("type", isEqualTo: 1)
        .getDocuments();
  }

  static Future<QuerySnapshot> getRequestAllWatting() async {
    return await Firestore.instance
        .collection("request")
        .orderBy("createdAt", descending: true)
        .where("type", isEqualTo: 0)
        .getDocuments();
  }

  static Future<void> updateRequest(String id, dynamic data) async {
    return await Firestore.instance
        .collection("request")
        .document(id)
        .updateData(data);
  }
}

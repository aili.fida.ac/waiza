import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/models/user.model.dart';
import 'package:waiza/src/views/screens/login/login.view.dart';

class UserService {
  static Future<void> setUser(UserModel _user) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString("user_waiza", json.encode(_user.toJson()));
  }

  static Future<void> logout(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.remove("user_waiza");
    _prefs.remove("phone_waiza");
    Helpers.goto(context, LoginView());
  }

  static Future<UserModel> getUser() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString("user_waiza") != null
        ? UserModel.fromJson(json.decode(_prefs.getString("user")))
        : null;
  }

  static void changePassword(BuildContext context, Firestore firestore,
      String password, String documentId) {
    UserService.getUser().then((value) {
      firestore
          .collection("users")
          .document(value.id)
          .updateData({"password": password}).then((value) {
        Toast.show("Votre mot de passe a bien été modifié", context);
      });
    });
  }

  static Future<void> setPhoneStorage(String _phone) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString("phone_waiza", _phone);
  }

  static Future getPhoneStorage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString("phone_waiza");
  }
}

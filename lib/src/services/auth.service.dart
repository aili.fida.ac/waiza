import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiza/src/constants/helpers.dart';
import 'package:waiza/src/views/screens/register/verifyMobile.view.dart';

class AuthService {
  static Future<void> sendCodeToPhoneNumber(
      BuildContext context, String phone) async {
    final PhoneVerificationCompleted verified = (AuthCredential authResult) {};

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      Toast.show("Something went wrong, please retry.", context);
    };

    final PhoneCodeSent codeSent = (String verificationId, [int forceResend]) {
      setPhoneNumber(phone).then((value) {
        return Helpers.goto(context, VerifyMobileView(verificationId),
            isReplaced: true);
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {};

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: const Duration(seconds: 30),
        verificationCompleted: verified,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: autoTimeout);
  }

  static Future<AuthResult> validation(
      BuildContext context, String code, String verificationID) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationID,
      smsCode: code,
    );

    return FirebaseAuth.instance.signInWithCredential(credential);
  }

  static Future<QuerySnapshot> login(String phone, String password) async {
    return await Firestore.instance
        .collection("users")
        .where("password", isEqualTo: password)
        .where("phone", isEqualTo: phone)
        .getDocuments();
  }

  static Future<QuerySnapshot> checkIfExiste(String phone) async {
    return await Firestore.instance
        .collection("users")
        .where("phone", isEqualTo: phone)
        .getDocuments();
  }

  static Future<DocumentReference> register(int userType, dynamic data) async {
    return await Firestore.instance.collection("users").add(data);
  }

  static Future<void> setPhoneNumber(String phone) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString("phonenumber", phone);
  }

  static Future<String> getPhoneNumber(String phone) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString("phonenumber");
  }
}

class UserModel {
  String id;
  String firstname;
  String lastname;
  String password;
  String phone;
  String username;
  String address;
  int userType;

  UserModel(
      {this.id,
      this.firstname,
      this.lastname,
      this.password,
      this.phone,
      this.address,
      this.username,
      this.userType});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return new UserModel(
        id: json['id'] as String,
        address: json['address'] as String,
        firstname: json['firstname'] as String,
        lastname: json['lastname'] as String,
        password: json['password'] as String,
        phone: json['phone'] as String,
        username: json['username'] as String,
        userType: json['userType'] as int);
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstname": firstname,
        "lastname": lastname,
        "password": password,
        "phone": phone,
        "address": address,
        "username": username,
        "userType": userType
      };
}

class RequestModel {
  String customer, depart, destination;
  DateTime createdAt, needAt;
  int type;

  RequestModel(
      {this.customer,
      this.depart,
      this.destination,
      this.createdAt,
      this.needAt,
      this.type});

  factory RequestModel.fromJson(Map<String, dynamic> json) {
    return new RequestModel(
        customer: json['customer'] as String,
        depart: json['depart'] as String,
        destination: json['destination'] as String,
        createdAt: json['date'] as DateTime,
        needAt: json['date'] as DateTime,
        type: json['type'] as int);
  }

  Map<String, dynamic> toJson() => {
        "customer": customer,
        "depart": depart,
        "destination": destination,
        "createdAt": createdAt,
        "needAt": needAt,
        "type": type
      };
}

import 'package:flutter/material.dart';
import 'package:waiza/src/views/screens/splash/splash.view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          typography: Typography.material2018(),
          fontFamily: "GoogleSans",
          textTheme: TextTheme(
            headline1: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 44,
            ),
            button: TextStyle(fontWeight: FontWeight.bold),
            headline2: TextStyle(fontWeight: FontWeight.w500, fontSize: 32),
            headline3: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
            headline4: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
            headline5: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            headline6: TextStyle(fontSize: 10, fontWeight: FontWeight.w700),
          )),
      home: SlpashView(),
    );
  }
}
